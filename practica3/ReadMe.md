Escribe un programa en C para Linux que acepte como argumentos una serie de direcciones IP o nombres de DNS. Por cada dirección o nombre, debe comprobar si responde a ARP haciendo un ping de un único mensaje y con timeout de 5 segundos. Para ello, deberá ejecutar el programa /bin/ping con los parámetros adecuados. 

Todos los pings se deben ejecutar en paralelo. El programa debe acabar con éxito solo si todos los pings han funcionado correctamente.

Por ejemplo, suponiendo que esas direcciones no responden a ping:

$> date
mar oct  9 10:20:19 CEST 2018
$> ./pinger 199.21.3.4  199.21.3.5  199.21.3.6
PING 199.21.3.5 (199.21.3.5) 56(84) bytes of data.

--- 199.21.3.5 ping statistics ---
1 packets transmitted, 0 received, 100% packet loss, time 0ms
PING 199.21.3.6 (199.21.3.6) 56(84) bytes of data.

--- 199.21.3.6 ping statistics ---
1 packets transmitted, 0 received, 100% packet loss, time 0ms
PING 199.21.3.4 (199.21.3.4) 56(84) bytes of data.

--- 199.21.3.4 ping statistics ---
1 packets transmitted, 0 received, 100% packet loss, time 0ms
$> echo $?
1
$> date
mar oct  9 10:20:25 CEST 2018
