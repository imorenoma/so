#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <err.h>

int stat;

void
make_ping(char *ip)
{
	switch (fork())
	{
		case -1:
			err(1,"fork");
		case 0:
			execl("/bin/ping","ping","-c","1","-W","5",ip,NULL);
			exit(EXIT_FAILURE);
	}
}

void
paralel(int len)
{
	int sts;
	int i;

	for(i=1; i<len;i++)
	{
		if(wait(&sts)<0)
		{
			err(1,"wait");
		}
		if(WEXITSTATUS(sts))
		{
				stat=EXIT_FAILURE;
		}
	}
}

int
main(int argc, char *argv[])
{
	int i;
	if(argc < 2)
	{
		fprintf(stderr, "Usage: %s %s\n",argv[0],"<Ip/dns> ...");
	}else{

		for(i=1;i<argc;i++)
		{
			make_ping(argv[i]);
		}
		paralel(argc);
	  exit(stat);
 	}
}
