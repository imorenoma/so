#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int boolean[100];
int repeated[100];

/*This function sort chars for less ASCII value
to high value*/

void
orderasc(char *str)
{
	int i;
	int j;
	char aux;
	int aux2;

	aux2 = strlen(str);

	for(i = 0; i < aux2-1; i++)
	{
		for(j= i+1; j< aux2; j++)
		{
			if (str[i] > str[j])
			{
				aux = str[i];
				str[i] = str[j];
				str[j] = aux;
			}
		}
	}
}

int
anagram(char *s1, char *s2)
{
	char str1[4096];
	char str2[4096];
	int len1;
	int len2;
	int i = 0;

	strncpy(str1, s1, 1024);
	strncpy(str2, s2, 1024);

	len1 = strlen(str1);
	len2 = strlen(str2);

	if(len1 != len2)
	{
		return 0; /*Not Anagrams*/
	}

	orderasc(str1);
	orderasc(str2);

	while(i < len1)
	{
		i++;
		if(str1[i]!=str2[i])
		{
			return 0;
		}
	}
	return 1;
}

void
schars(int pos, int argc, char *argv[])
{
	int i;
	int j;
	char *str;
	char *str2;
	int len;

	 str = argv[pos];
	 len = strlen(str);

	 printf("[");

	 for(i=0;i<len;i++)
	 {
		 for(j=pos+1;j<argc;j++)
		 {
			 str2=argv[j];
			 if (boolean[j])
			 {
				 if (str[i]!=str2[i])
				 {
					 printf("%c",str[i]);
					 break;
				 }
			 }
		 }
	 }
	 printf("]\n");
 }

void
check_an(int pos,int argc,char *argv[])
{
	int i;
	int found = 0;

	for(i =0; i < 100; i++)
	{
		boolean[i]=0;
	}

	for(i = pos+1;i < argc;i++)
	{
		found=0;

		for(i =0; i < 100; i++)
 	 	{
 			boolean[i]=0;
 	 	}

	 for(i=pos+1;i<argc;i++)
	 {
		 if (anagram(argv[pos],argv[i]))
		 {
			 boolean[pos]=1;
			 boolean[i]=1;
			 repeated[i]=1;
			 found=1;
		 }
	 }

	 while(found)
	 {
		 for(i=0;i<argc;i++)
		 {
			 if (boolean[i])
			 printf("%s ",argv[i]);
		 }
		 schars(pos,argc,argv);
		 break;
	 	}
	}
}

int
main(int argc, char *argv[]) {

	int i;

	if(argc < 3 || argc > 100)
	{
		printf("Bad number of arguments\n");
		printf("Usage: ./anagrams <word1> <word2> ... <word100> \n");
		exit(EXIT_FAILURE);

	}else{

		for(i=0; i < 100; i++)
		{
			boolean[i]=0;
		}

		for(i = 1; i < argc-1; i++)
		{
			if(!repeated[i])
			{
				check_an(i,argc,argv); /*compare two possition */
			}
		}
	}
	exit(EXIT_SUCCESS);
}
