Anagrams: Cálculo de anagramas

Escriba un programa en C para linux imprima una línea por cada conjunto de
anagramas extraído de los argumentos, que sólo contendrá caracteres ascii.
Debe indicar al final de la línea entre corchetes las letras que no están en
la misma posición en todos. Considere que como mucho hay 100 parámetros.

Por ejemplo:

$> ./anagrams hola peralo aloh pelaro tras tiki polare
   hola aloh [hola]
   peralo pelaro polare [erlo]
$>


    Los argumentos que no tienen anagramas no deben imprimirse.
    El programa consistirá de un único fichero: anagrams.c.
