Escriba un programa en C para linux que admita como argumentos nombres de variables de entorno. El programa debe escribir por su salida, uno por línea, cada elemento que contienen las variables de entorno especificadas. Se considera que los elementos de una variable de entorno están separados por el carácter ':' (como en el caso de la variable de entorno PATH). Si alguna de las variables especificadas no existe, el programa debe terminar con fallo.

Por ejemplo:

$> echo $PATH
/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/usr/local/go/bin/:/opt/puppetlabs/bin/:./:/snap/bin
$> echo $XDG_DATA_DIRS
/usr/local/share:/usr/share:/var/lib/snapd/desktop
$> ./elems PATH XDG_DATA_DIRS
/usr/local/sbin
/usr/local/bin
/usr/sbin
/usr/bin
/sbin
/bin
/usr/games
/usr/local/games
/usr/local/go/bin/
/opt/puppetlabs/bin/
./
/snap/bin
/usr/local/share
/usr/share
/var/lib/snapd/desktop
$> export hola=adios
$> ./elems hola PATATA
adios
ERROR: var PATATA does not exist
$> echo $?
1
$> 


