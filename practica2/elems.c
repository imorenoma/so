#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <err.h>

/*function to tokenize the value of
the enviroment variable*/
enum{MAXNUM=100};

int
token(char *tpath)
{
		char *tokenize;
		char *p1 = tpath;
		char delimiter[]=":";

		while ((tokenize = strtok_r(p1,delimiter,&p1)))
		{
			printf("%s\n", tokenize);
		}
		return 0;
}

/*Funtion to read argumets to send it at
token function*/

int
is_var(int argc, char *argv[])
{
	int i;
	char *var[MAXNUM];

	for(i=1; i<argc; i++)
	{
		var[i] = getenv(argv[i]);

		if(var[i] != NULL)
		{
			token(var[i]);
			printf("\n");
		}else{
			//warnx("Error: var %s %s\n", argv[i]," does not exist"); other way to print err
			fprintf(stderr, "ERROR: var %s %s\n", argv[i], "does not a variable");
			return -1;
		}
	}
	printf("\n");
	return 0;
}

int
main(int argc, char *argv[]) {

	if(argc < 2)
	{
		//warnx("Usage: %s <var1> ... \n",argv[0]); other way to print err
		fprintf(stderr, "Usage: %s %s\n",argv[0],"<var1> <var2> ...");
		exit(EXIT_FAILURE);
	}else{
		printf("\n");
		is_var(argc, argv);
	}
	exit(EXIT_SUCCESS);
}
