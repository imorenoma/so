#include <stdio.h>
#include <stdlib.h>


int
main(int argc, char *argv[])
{
	if (argc == 1)
	{
		printf("Bad number of arguments \n");
		printf("Usage ./argument <argument> \n");
		exit(EXIT_FAILURE);

	}else{

		for (int i = 0; i<argc; i++)
		{
			printf("Argument: %d : %s \n",i, argv[i]);
		}
		exit(EXIT_SUCCESS);
	}

}
