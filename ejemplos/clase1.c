#include <stdio.h>
#include <stdlib.h>

/*this program runs some examples of the first class of
operative systems*/

/*
gcc -c -Wall -Wshadow -g <fichero.c> --compile
gcc -o  fichero fichero.o --link & exec
*/

/*Hello world program*/

int main(int argc, char *argv[])
{
	printf("Hello World !!!\n");
	exit(EXIT_SUCCESS);
}

/*---------------------------*/
/*---------------------------*/
/*---------------------------*/

/*
int x = 1;
int k;

int
main(int argc, char *argv[]) {

	int i, q=1, u=12;
	char c;
	char p = 'o';

	c = 'z';
	i = 13;

	exit(EXIT_SUCCESS);
}
*/
