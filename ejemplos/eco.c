#include <stdlib.h>
#include <stdio.h>

/*This program shows the argument index
 and the argument*/

int main(int argc, char* argv[]) {
	int i;

	for(i = 0; i < argc; i++)
	{
			printf("%d\t%s\n", i, argv[i] );
	}
	return 0;
}
