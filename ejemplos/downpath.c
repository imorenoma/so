#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <stdio.h>


static const int bufflen = 513;


char*
upper(char *str)
{
	int i;

	for(i = 0; i < strlen(str) ; i++)
	{
		str[i] = toupper(str[i]);
	}
	return str;
}


int
makepath(char *buf, int len)
{
	int maxsize = 100;
	char *opath = getenv("HOME");
	char *name = getenv("USER");

	if (opath == NULL) /*check the path */
	{
		return -1;
	}

	strncat(buf,opath, maxsize);
	strncat(buf, "/Dowloads/", maxsize);

	if (name == NULL) /* check name */
	{
		return -1;
	}
	else
	{
		char *uppername = upper(name);
		strncat(buf, uppername, maxsize);
	}

	int npid = getpid();
	char cpid[len];

	sprintf(cpid, "%d", npid); /*cast integer to string*/
	strncat(buf, ".", 5);
	strncat(buf, cpid, 30);

	if (strlen(buf) > bufflen)
	{
		return -1;
	}
	else
	{
		return(strlen(buf));
	}
}

int
main(int argc, char *argv[])
{
	int length = 30;

	if(argc != 1)
	{
		printf("Bad number of arguments\n");
		printf("usage: .\\downpath\n");
		return -1;
	}

	char *buffer = malloc(bufflen *sizeof(char));

	if(makepath(buffer,length) < 0)
	{
		free(buffer);
		return -1;
	}

	printf("%s\n", buffer);
	free(buffer);

	return 0;
}
