#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <err.h>

int stat;

void
make_ping(char *ip)
{
    switch(fork())
    {
        case -1:
            fprintf(stderr, "%s\n", "fork");
        case 0:
            execl("/bin/ping","ping","-c","5","-W","5",ip);
            exit(EXIT_FAILURE);
    }        
}

void
parallel(int len)
{
    int sts
    int i;

    for(i=0;i<len;i++)
    {
        if(wait(&sts)<0)
        {
            err(1,"wait");
        }
        if(WEXITSTATUS(sts))
        {
            stat=EXIT_FAILURE;
        }
    }
}

int main(int argc, char *argv[]) {
   
    int i;
    int bufflen = 1024;
    char *concat[];
    char buff = (char *)malloc(bufflen *sizeof(char));

    for(i=0; i<argc; i++)
    {
        if(buff == NULL)
        {
            fprintf(stderr, "%s\n","Memmory error");
            free(buff);
        }

        concat = strcpy(buff,argv[i]);

        make_ping(concat);
    }

    parallel(argc);
    free(buff);
    exit(stat);
}